package net.daum.mail;

import java.util.ArrayList;
import java.util.List;

import javax.activation.android.MimetypesFileTypeMap;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Message.RecipientType;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SMTPMailSender implements MailSender {

	private String subject;
	private Address from;
	private List<Address> toList = new ArrayList<Address>();
	private List<Address> ccList = new ArrayList<Address>();
	private List<Address> bccList = new ArrayList<Address>();
	
	private String text;
	private List<MailInlineAttachment> images = new ArrayList<MailInlineAttachment>();
	private List<MailAttachment> attachments = new ArrayList<MailAttachment>();
	
	private SMTPClient client;
	
	public SMTPMailSender(SMTPClient client) {
		this.client = client;
		System.setProperty("mail.mime.encodefilename", "true");
	}
	
	@Override
	public void send() throws MessagingException {
		client.send(makeMessage());
	}

	private Message makeMessage() throws MessagingException {
		MimeMessage message = new MimeMessage(client.getSession());
		message.setSubject(subject, "UTF-8");
		message.setFrom(from);
		message.setRecipients(RecipientType.TO, toList.toArray(new Address[toList.size()]));
		message.setRecipients(RecipientType.CC, ccList.toArray(new Address[ccList.size()]));	
		message.setRecipients(RecipientType.BCC, bccList.toArray(new Address[bccList.size()]));	

		if (images.size() > 0 || attachments.size() > 0) {
			MimeMultipart multipart = null;
			MimeBodyPart body = new MimeBodyPart();
			body.setText(text);
			body.setHeader("Content-Type", "text/html;charset=UTF-8");
			
			if (images.size() > 0) {
				multipart = new MimeMultipart("related");
				multipart.addBodyPart(body);
				
				for (MailInlineAttachment image:images) {
					multipart.addBodyPart(image.toMimeBodyPart());
				}
				MimeBodyPart mr = new MimeBodyPart();
				mr.setContent(multipart);
				body = mr;
			}
			
			if (attachments.size() > 0) {
				multipart = new MimeMultipart("mixed");
				multipart.addBodyPart(body);
				
				for (MailAttachment attachment:attachments) {
					multipart.addBodyPart(attachment.toMimeBodyPart());
				}
			}
			message.setContent(multipart);
		} else {
			message.setText(text);
		}
		
		return message;
	}

	@Override
	public void setFrom(Address from) {
		this.from = from;
	}

	@Override
	public void addTo(Address to) {
		this.toList.add(to);
	}

	@Override
	public void addCc(Address cc) {
		this.ccList.add(cc);
	}

	@Override
	public void addBcc(Address bcc) {
		this.bccList.add(bcc);
	}

	@Override
	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public void addAttachment(String realPath, String filename) {
		MailAttachment attachment = new MailAttachment(realPath, filename, MimetypesFileTypeMap.getDefaultFileTypeMap().getContentType(filename));
		attachments.add(attachment);
	}

	@Override
	public void addImage(String realPath, String filename, String contentId) {
		MailInlineAttachment image = new MailInlineAttachment(realPath, filename, MimetypesFileTypeMap.getDefaultFileTypeMap().getContentType(filename), contentId);
		images.add(image);
	}

}
