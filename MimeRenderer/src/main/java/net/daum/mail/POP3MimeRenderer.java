package net.daum.mail;

import javax.mail.Message;

public class POP3MimeRenderer extends MimeRendererImpl {

	public POP3MimeRenderer(Message message) {
		super(message);
	}
	public POP3MimeRenderer(Message message, String filePrefix) {
		super(message, filePrefix);
	}

	public POP3MimeRenderer(Message message, String filePrefix, String baseDir) {
		super(message, filePrefix, baseDir);
	}
	
}
