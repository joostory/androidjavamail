package net.daum.mail.exception;

public class MimeRenderException extends Exception {

	private static final long serialVersionUID = -622903232634119238L;

	public MimeRenderException(Throwable t) {
		super("Cannot render message", t);
	}

}
