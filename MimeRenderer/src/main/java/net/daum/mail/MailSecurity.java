package net.daum.mail;

public enum MailSecurity {

    SSL("ssl"), TLS("tls"), OAUTH2("oauth2");

    private String value;

    private MailSecurity(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static MailSecurity parse(String value) {
        if (SSL.getValue().equals(value)) {
            return SSL;
        } else if (TLS.getValue().equals(value)) {
            return TLS;
        } else if (OAUTH2.getValue().equals(value)) {
            return OAUTH2;
        } else {
            throw new IllegalStateException("Unknown value : " + value);
        }
    }
}
