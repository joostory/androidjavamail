package net.daum.mail;

import javax.activation.android.DataHandler;
import javax.activation.android.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;

public class MailAttachment {
	
	protected String realPath;
	protected String filename;
	protected String contentType;
	protected String contentDisposition = Part.ATTACHMENT;

	public MailAttachment(String realPath, String filename, String contentType) {
		this.realPath = realPath;
		this.filename = filename;
		this.contentType = contentType;
	}

	public String getRealPath() {
		return realPath;
	}

	public void setRealPath(String realPath) {
		this.realPath = realPath;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	public String getContentDisposition() {
		return contentDisposition;
	}

	public void setContentDisposition(String contentDisposition) {
		this.contentDisposition = contentDisposition;
	}

	public MimeBodyPart toMimeBodyPart() throws MessagingException {
		MimeBodyPart part = new MimeBodyPart();
		part.setHeader("Content-Type", contentType);
		part.setFileName(filename);
		part.setDisposition(contentDisposition);
		part.setDataHandler(new DataHandler(new FileDataSource(realPath)));
		return part;
	}
}
