package net.daum.mail;

import javax.mail.Address;
import javax.mail.MessagingException;

public interface MailSender {

	public void send() throws MessagingException;

	public void setFrom(Address from);
	public void addTo(Address to);
	public void addCc(Address cc);
	public void addBcc(Address bcc);
	
	public void setSubject(String subject);
	public void setText(String text);
	public void addAttachment(String realPath, String filename);
	public void addImage(String realPath, String filename, String contentId);
	
}
