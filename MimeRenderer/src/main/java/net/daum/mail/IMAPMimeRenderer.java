package net.daum.mail;

import javax.mail.Message;

public class IMAPMimeRenderer extends MimeRendererImpl {

	public IMAPMimeRenderer(Message message) {
		super(message);
	}

	public IMAPMimeRenderer(Message message, String filePrefix) {
		super(message, filePrefix);
	}

	public IMAPMimeRenderer(Message message, String filePrefix, String baseDir) {
		super(message, filePrefix, baseDir);
	}

}
