package net.daum.mail;

import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;

public class MailInlineAttachment extends MailAttachment {

	protected String contentId;
	
	public MailInlineAttachment(String realPath, String filename, String contentType, String contentId) {
		super(realPath, filename, contentType);
		this.contentDisposition = Part.INLINE;
		this.contentId = contentId;
	}
	
	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	@Override
	public MimeBodyPart toMimeBodyPart() throws MessagingException {
		MimeBodyPart part = super.toMimeBodyPart();
		part.setContentID("<" + contentId + ">");
		return part;
	}

}
