package net.daum.mail;

import java.util.List;

import javax.mail.Message;
import javax.mail.Part;

import net.daum.mail.exception.MimeRenderException;

public interface MimeRenderer {

	public void render() throws MimeRenderException;
	public void renderForAttachment() throws MimeRenderException;
	
	public void saveText() throws Exception;
	
	public void saveAttachment(int index) throws Exception;
	
	public String getText();
	public Message getMessage();
	public List<Part> getAttachments();
}
