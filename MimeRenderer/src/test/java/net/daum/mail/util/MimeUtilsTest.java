package net.daum.mail.util;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.mail.internet.MimeUtility;
import javax.mail.internet.ParseException;

import org.junit.Test;

import com.sun.mail.util.ASCIIUtility;
import com.sun.mail.util.BASE64DecoderStream;
import com.sun.mail.util.QDecoderStream;

public class MimeUtilsTest {

	@Test
	public void testGetBaseType() {
		String target = "text/html; charset=utf-8";
		String result = MimeUtils.getBaseType(target);
		assertEquals("text/html", result);
	}
	
	@Test
	public void testGetParameter() {
		String target = "text/html; charset=utf-8";
		String result = MimeUtils.getParameter(target, "charset");
		assertEquals("utf-8", result);
	}
	
	@Test
	public void testGetPrimaryType() {
		String target = "text/html; charset=utf-8";
		String result = MimeUtils.getPrimaryType(target);
		assertEquals("text", result);
	}
	
	@Test
	public void testGetSubType() {
		String target = "text/html; charset=utf-8";
		String result = MimeUtils.getSubType(target);
		assertEquals("html", result);
	}
	
	@Test
	public void testDecode() throws UnsupportedEncodingException, ParseException {
		String target = "=?utf-8?B?WGJveCBMaXZlIDHqsJzsm5Qg7ISg67aIIOqzqOuTnCDrqaTr?= =?utf-8?B?soTsi60g7Lm065OcIOunjOujjCDslYzrprw=?=";
		String result = MimeUtility.decodeText(target);
		assertEquals("Xbox Live 1개월 선불 골드 멤버십 카드 만료 알림", result);
	}
	
	@Test
	public void testDecodeWithPadding() throws ParseException, IOException {
		String target = "=?UTF-8?B?RmFjZWJvb2sg7IOB7JeQ64qUIO2ajOybkA==?= =?UTF-8?B?64uY7J2YIOy5nOq1rOuTpOydtCDrjZQg66eO7J20IOyeiOyKtQ==?= =?UTF-8?B?64uI64ukLg==?=";
		String result = MimeUtility.decodeText(target);
		assertEquals("Facebook 상에는 회원님의 친구들이 더 많이 있습니다.", result);
	}
	
}
