package net.daum.mail.util;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.mail.internet.InternetAddress;

import org.junit.Test;

public class MimeStringUtilsTest {

	@Test
	public void testCleanHtml() {
		String target = "<p>Hello</p>";
		String result = MimeStringUtils.cleanHtml(target);
		assertEquals("Hello", result);
	}
	
	@Test
	public void testPlainToHtml() {
		String target = "Hello!\r\nHi!";
		String result = MimeStringUtils.plainToHtml(target);
		assertEquals("Hello!<br>Hi!", result);
	}
	
	@Test
	public void testAddressesToUnicodeString() throws UnsupportedEncodingException {
		InternetAddress[] target = {
			new InternetAddress("test@test.com", "sample"), new InternetAddress("test2@test2.com", "sample2") 
		};
		String result = MimeStringUtils.addressesToUnicodeString(target);
		assertEquals("sample <test@test.com>,sample2 <test2@test2.com>", result);
	}
	
	@Test
	public void testGetStringFromInputStream() {
		String expected = "Test String";
		InputStream target = new ByteArrayInputStream(expected.getBytes());
		String result = MimeStringUtils.getStringFromInputStream(target);
		assertEquals(expected, result);
	}
}
