package javax.mail.internet;

import static org.junit.Assert.*;

import org.junit.Test;

public class ContentTypeTest {

	@Test
	public void parse() {
		System.setProperty("mail.mime.parameters.strict", "false");
		
		try {
			new ContentType("text/html charset=utf8");
			new ContentType("text/html; charset utf8");
			new ContentType("text/html; charsetutf8");
		} catch (ParseException e) {
			fail(e.getMessage());
		}
	}
	
}
