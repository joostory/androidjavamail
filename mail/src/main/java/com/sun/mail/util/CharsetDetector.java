package com.sun.mail.util;

import org.mozilla.universalchardet.UniversalDetector;

public class CharsetDetector {

	public static String detect(byte[] buf) {
		String charset = null;
		try {
			UniversalDetector detector = new UniversalDetector(null);
	        detector.handleData(buf, 0, buf.length);
	        detector.dataEnd();

	        charset = detector.getDetectedCharset();
	        detector.reset();
		} catch (Exception e) {
			// 
			e.printStackTrace();
		}
		
		if (charset == null) {
			charset = System.getProperty("mail.default.charset", "utf-8");
		}
        return charset;
	}
	
}
