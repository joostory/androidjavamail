package com.sun.mail.imap.protocol;

public class MID implements Item {

	static final char[] name = {'M','I','D'};
	public int msgno;
	public String mid;
	
	public MID(IMAPResponse r) {
		msgno = r.getNumber();

		r.skipSpaces();
		
		mid = r.readAtomString();
	}

}
