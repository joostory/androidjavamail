package com.sun.mail.imap.protocol;

import java.io.UnsupportedEncodingException;

import javax.mail.internet.MimeUtility;

public class FOLDER implements Item {

	static final char[] name = {'F','O','L','D','E','R'};
	public int msgno;
	public String folder;
	
	public FOLDER(IMAPResponse r) {
		msgno = r.getNumber();

		r.skipSpaces();
		
		String val = r.readString();
		
		try {
			folder = MimeUtility.decodeText(val);
		} catch (UnsupportedEncodingException e) {
			folder = val;
		}
	}
	
}
