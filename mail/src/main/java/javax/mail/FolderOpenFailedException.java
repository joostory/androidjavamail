package javax.mail;

public class FolderOpenFailedException extends MessagingException {
	transient private Folder folder;
	
	private static final long serialVersionUID = 6735934779979537196L;

	public FolderOpenFailedException(Exception e) {
		super("Folder open failed", e);
	}

	public FolderOpenFailedException(Folder folder, Exception e) {
		super(folder + " open failed", e);
		this.folder = folder;
	}
	
	public Folder getFolder() {
		return folder;
	}
}
