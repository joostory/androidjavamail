package javax.mail;

public class NoPasswordException extends AuthenticationFailedException {

	private static final long serialVersionUID = 2255105761308859271L;

	public NoPasswordException() {
		super("failed to connect, no password specified?");
	}
}
