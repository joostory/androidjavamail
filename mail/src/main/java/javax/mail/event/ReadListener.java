package javax.mail.event;

public interface ReadListener extends java.util.EventListener {
	
	public void read(long size);

	public void start();

	public void end();
	
}
