package javax.mail.event;

public interface WriteListener extends java.util.EventListener {

	public void start();
	public void end();
	public void write(long size);
	
}
