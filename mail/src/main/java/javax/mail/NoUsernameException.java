package javax.mail;

public class NoUsernameException extends AuthenticationFailedException {

	private static final long serialVersionUID = 5725069965589694804L;

	public NoUsernameException() {
		super("failed to connect, no user name specified?");
	}
	
}
