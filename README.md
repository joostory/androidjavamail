Android Javamail
================

Android Javamail은 Oracle Javamail의 Android 포팅 버전입니다. Android에 포함되지 않은 awt와 JAF의 일부 모듈을 포함하였습니다.   

# Build
gradle을 사용해 jar로 build 가능하다

## clean
    gradle clean

## build jar
    // 모든 product의 jar 생성
    gradle jar
    
    // 각 product별 jar 생성
    gradle :activation:jar
    gradle :additionnal:jar
    gradle :mail:jar
    gradle :MimeRenderer:jar

# Lisence
- [CDDL-1.1](https://glassfish.java.net/public/CDDL+GPL_1_1.html)
- [GPL-2.0](http://www.gnu.org/licenses/gpl-2.0.html)